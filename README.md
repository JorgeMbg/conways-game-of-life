## Proyecto 2: El juego de la vida

## Descripción
Programa que simula el juego de la vida de Jonh Horton Conway.

## Requisitos 
##### Sistema operativo Linux (Debian / Ubuntu / CentOS) o Windows con Git #####
##### Java 11.0.8 2020-07-14 & Java(TM) SE Runtime Environment 18.9 (build 11.0.8+10-LTS). #####
```BASH
sudo apt-get update  
sudo apt install default-jre
```
##### Git 2.28.0 instalado #####
```BASH
sudo apt-get update  
sudo apt-get install git -y
```
##### Instalar make:#####
```BASH
sudo apt-get install build-essential
```
##### Realizar un git clone del repositorio: #####
```BASH
git clone https://JorgeMbg@bitbucket.org/JorgeMbg/conways-game-of-life.git
```
##### Ubicarnos en la carpeta del programa: #####
```BASH
cd conways-game-of-life
```
##### Compilar el programa con: #####
```BASH
make compilar
```
##### Crear el ejecutable JAR: #####
```BASH
make jar
```
### Diagrama del programa ###

### Instrucciones de uso ###
#### Generar Aproximacion de PI ####
```BASH
```

### Desarrollador ###

* Jorge Martin-Benito Garcia

### Licencia ###

![https://bitbucket.org/JorgeMbg/conways-game-of-life/src/master/img/logo_cc.jpg](https://i.gyazo.com/d5c2f506fdd9e679b02c9bfaa28193e3.png)

Type: CC BY,
This license lets others distribute, remix, adapt, and build upon your work, even commercially, as long as they credit you for the original creation. This is the most accommodating of licenses offered. Recommended for maximum dissemination and use of licensed materials.