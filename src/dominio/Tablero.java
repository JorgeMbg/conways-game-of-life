package dominio;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.management.openmbean.OpenDataException;
/**
* Esta clase es responsable de leer el tablero de un
* fichero en forma de ceros y unos, ir transitando de
* estados e ir mostrando dichos estados.
*/

public class Tablero{
    private static int DIMENSION = 30;
    private int[][] estadoActual; //matriz que representa el estado actual.
    private int[][] estadoSiguiente = new int[DIMENSION][DIMENSION]; // Matriz que representa el estado siguiente.

/********************************************************
* Lee el estado inicial de un fichero llamado ‘matriz‘.
********************************************************/  
    
    public void leerEstadoActual(){
        String matriz = "matriz.txt";
        try {
            File fichero = new File (matriz);
            Scanner sc = new Scanner(fichero);
            int i =0;
            while(sc.hasNext()){
                Tablero tablero = new Tablero();
                String linea = sc.nextLine();
                    for (int j = 1; j < 30; j++) 
                    { 
                        estadoActual[i][j] = linea.charAt(j);
                    } 
                    i++;
                
            }
        } catch(IOException ex) {
            System.err.println(ex);
        }
    }

    
    // La secuencia de ceros y unos del fichero es guardada
    // en ‘estadoActual‘ y, utilizando las reglas del juego
    // de la vida, se insertan los ceros y unos
    // correspondientes en ‘estadoSiguiente‘.

/********************************************************
* Genera un estado inicial aleatorio. Para cada celda
* genera un número aleatorio en el intervalo [0, 1). Si
* el número es menor que 0,5, entonces la celda está
* inicialmente viva. En caso contrario, está muerta.
*******************************************************/
    
    public static int generarEstadoIncialAleatorio(){
        if (Math.random() > 0.5) {
          return 1;
        }
        else{
            return 0;
        }
    }
    

    public int[][] generarEstadoActualPorMontecarlo() {
        
        int M = 0;
        int N = 0;
        int [][] matrizNueva = new int [M][N];
            
        for (int i = 1; i < M-1; i++) 
        { 
            for (int j = 1; j < N-1; j++) 
            { 
                matrizNueva [i][j] = generarEstadoIncialAleatorio();
            } 
        }
        return matrizNueva;
    }
    
    // La secuencia de ceros y unos generada es guardada
    // en ‘estadoActual‘ y, utilizando las reglas del juego
    // de la vida, se insertan los ceros y unos
    // correspondientes en ‘estadoSiguiente‘.

/********************************************************
* Transita al estado siguiente según las reglas del
* juego de la vida.
********************************************************/

    public static int [][] transistarAlEstadoSiguiente(int tablero[][]){ 

        int M = 30;
        int N = 30;
        int[][] futuro = new int[M][N];
        
		for (int fila = 1; fila < M - 1; fila++){ 
			for (int columna = 1; columna < N - 1; columna++){ 

                int celulasVecinasVivas = 0; 
				for (int i = -1; i <= 1; i++) 
					for (int j = -1; j <= 1; j++) 
						celulasVecinasVivas += tablero[fila + i][columna + j]; 

				celulasVecinasVivas -= tablero[fila][columna]; 

			} 
		} 
                return futuro;
	} 
    // La variable ‘estadoActual‘ pasa a tener el contenido
    // de ‘estadoSiguiente‘ y, éste útimo atributo pasar a
    // reflejar el siguiente estado.

/*******************************************************
* Devuelve, en modo texto, el estado actual.
* @return el estado actual.
*******************************************************/
    @Override
    public String toString(){
        String salida = "";

        for( int i = 0; i < DIMENSION - 1; i++) {
            for (int j =0; j < DIMENSION -1; j++) {
                estadoActual[i][j]=1;
            }
        }
        
        return salida;
    }
}
